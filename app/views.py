# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render,redirect,get_object_or_404
from django.shortcuts import render_to_response
import requests
from datetime import datetime
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.core.files.storage import FileSystemStorage
from django import forms
import simplejson
import itertools

from django import forms
#forms classes
from app.forms import Emlak_bilgileri_Form,Filtre_Form
from app.forms import Profil_info_Form
from app.forms import DocumentForm
from app.forms import Register_Form

#model classes

from app.models import Emlak_bilgileri
from app.models import Ev_kolayliklari
from app.models import Profil_info
from app.models import Konut_Tipi
from app.models import Oda_Tipi
from app.models import Cinsiyet
from app.models import Mulk_kurallari
from app.models import Banyo_tipi
from app.models import Galeri
from app.models import Dokumanlar
from app.models import Profil_ranked
from django.contrib.auth.models import User
# Create your views here.
def index(request):
	#galeri=Galeri.objects.all()
	#dokuman=Dokumanlar.objects.all()
	kontrol = 0
	kisi_s=User.objects.all().count()
	ilan_s=Emlak_bilgileri.objects.all().count()
	zoom = 6
	form=Filtre_Form()
	return render(request,'g_anasayfa.html',{'kontrol':kontrol,
		'zoom':zoom,
		'galeri':"a",
		'dokuman':"a",
		'adres_listesi':"a",
		'baslik_listesi':"a",
		'id_listesi':"a",
		'ucret':"a",
		'oda':"a",
		'alan':"a",
		'tarih':"a",
		'g_kapak':"a",
		'okul':"a",
		'form':form,
		'kisi_s':kisi_s,
		'ilan_s':ilan_s
		})
	#anasayfa
def Search(request):
	if request.method == "GET":
		q=""
		universite =request.GET.get('universite')
		fakulte = request.GET.get('fakulte')
		text = request.GET.get("ara")
		a_ucret = request.GET.get('ucret')
		genislik=request.GET.get('genislik')
		oda_t=request.GET.get('oda_tipi')

		konut_t=request.GET.get('konut_tipi')
		banyo_t=request.GET.get('banyo_tipi')
		cinsiyet=request.GET.get('cinsiyet')
		universite=request.GET.get("universite")
		fakulte=request.GET.get("fakulte")
		oda_sayisi=request.GET.get('oda_sayisi')
		yatak_sayisi=request.GET.get('yatak_sayisi')

		baslik="Arama sonucu:"
		mesaj=" "
		em_bil = Emlak_bilgileri.objects.filter(Q(sehir__icontains = text) | Q(ilce__icontains = text))
		if universite and fakulte:
			okul=universite+" "+fakulte;
			q+="&universite="+universite+"&fakulte="+fakulte
		else :
			if em_bil:
				okul=em_bil[0].sehir
			else:
				okul=text
		q+="&ara="+text
		if genislik:
			em_bil=em_bil.filter(genislik__gte=int(genislik.split(",")[0]) , genislik__lte=int(genislik.split(",")[1]))
			q+="&genislik="+genislik
		if a_ucret:
			em_bil=em_bil.filter(aylik_ucret__gte=float(a_ucret.split(",")[0]) , aylik_ucret__lte=float(a_ucret.split(",")[1]))
			q+="&ucret="+a_ucret
		if oda_sayisi:
			em_bil=em_bil.filter(oda_sayisi__gte=int(oda_sayisi.split(",")[0]) , oda_sayisi__lte=int(oda_sayisi.split(",")[1]))
			q+="&oda_sayisi="+oda_sayisi
		if yatak_sayisi:
			em_bil=em_bil.filter(yatak_sayisi__gte=int(yatak_sayisi.split(",")[0]) , yatak_sayisi__lte=int(yatak_sayisi.split(",")[1]))
			q+="&yatak_sayisi="+yatak_sayisi
		if oda_t:
			em_bil=em_bil.filter(oda_tipi = oda_t)
			q+="&oda_tipi="+oda_t
		if konut_t:
			em_bil=em_bil.filter(konut_tipi = konut_t)
			q+="&konut_tipi="+konut_t
		if banyo_t:
			em_bil=em_bil.filter(banyo_tipi = banyo_t)
			q+="&banyo_tipi="+banyo_t
		if cinsiyet:
			em_bil=em_bil.filter(cinsiyet = cinsiyet)
			q+="&cinsiyet="+cinsiyet
		adres_=[]
		baslik_=[]
		id_=[]
		g_id_=[]
		g_kapak_=[]
		galeri_list=[]
		alan_=[]
		oda_=[]
		ucret_=[]
		tarih_=[]
	
		for x in em_bil:
			adres_.append(x.adres1+" "+x.ilce+"/"+x.sehir)
			baslik_.append(x.ilan_baslik)
			id_.append(x.id)
			alan_.append(x.genislik)
			oda_.append(x.oda_sayisi)
			ucret_.append(x.aylik_ucret)
			tarih_.append(str(x.baslangic_tarihi).split(" ")[0])
			galeri_list.append(Galeri.objects.get(e_bilgileri=x.id))

		for x in galeri_list:
			g_kapak_.append(x.kapak_image.url)
		for  i in galeri_list:
			i.e_bilgileri.baslangic_tarihi=str(i.e_bilgileri.baslangic_tarihi).split(" ")[0]
			if len(i.e_bilgileri.ilan_baslik) >40:
				i.e_bilgileri.ilan_baslik=i.e_bilgileri.ilan_baslik[:40]+"..."
		tarih=simplejson.dumps(tarih_)
		ucret=simplejson.dumps(ucret_)
		oda=simplejson.dumps(oda_)
		alan=simplejson.dumps(alan_)
		g_kapak=simplejson.dumps(g_kapak_)
		adres_listesi = simplejson.dumps(adres_)
		baslik_listesi = simplejson.dumps(baslik_)
		id_listesi = simplejson.dumps(id_)
		kontrol = 1

		if len(adres_)==0:
			em_bil=""
			mesaj=str(text.encode('utf-8'))+" konumunda şarta uygun ilan bulunamadı!"
			zoom=6
		else:
			mesaj=str(text.encode('utf-8'))+" adresindeki ilanlar"
			zoom = 12
		page = request.GET.get('page', 1)
		paginator = Paginator(galeri_list, 10)
		try:
			galeri_list = paginator.page(page)
		except PageNotAnInteger:
			galeri_list = paginator.page(1)
		except EmptyPage:
			galeri_list = paginator.page(paginator.num_pages)
		form=Filtre_Form()
		return render(request,'g_anasayfa.html',{'em_bil':em_bil,
			'galeri_list':galeri_list,
			'baslik':baslik,
			'adres_listesi':adres_listesi,
			'baslik_listesi':baslik_listesi,
			'id_listesi':id_listesi,
			'zoom':zoom,
			'mesaj':mesaj,
			'ucret':ucret,
			'oda':oda,
			'alan':alan,
			'tarih':tarih,
			'kontrol':kontrol,
			'g_kapak':g_kapak,
			'q':q,
			'text':text,
			'a_ucret':a_ucret,
			'okul':okul,
			'form':form
			})


	return HttpResponseRedirect('/')
@login_required
def Form(request):
	

	if request.method == 'POST':
		form=Emlak_bilgileri_Form(request.POST or None)

		if form.is_valid():
			tarih=request.POST.get('baslangic_tarihi')
			
			eb=Emlak_bilgileri()
			eb.profil_id=request.user.id
			eb.ilan_detayii=request.POST.get('ilan_detayii',"")
			eb.ilan_baslik=request.POST.get('ilan_baslik')
			eb.adres1=request.POST.get('adres1')
			eb.adres2=request.POST.get('adres2',"")
			eb.ilce=request.POST.get('ilce')
			eb.sehir=request.POST.get('sehir')
			eb.posta_kodu=request.POST.get('posta_kodu',"")
			eb.oda_sayisi=int(request.POST.get('oda_sayisi'))
			eb.mutfak_sayisi=int(request.POST.get('mutfak_sayisi'))
			eb.banyo_sayisi=int(request.POST.get('banyo_sayisi'))
			eb.salon_sayisi=int(request.POST.get('salon_sayisi'))
			eb.ilan_tarihi=datetime.now()
			eb.website=request.POST.get('website',"")
			eb.yatak_oda_sayi=int(request.POST.get('yatak_oda_sayi'))
			eb.yatak_sayisi=int(request.POST.get('yatak_sayisi'))
			eb.genislik=int(request.POST.get('genislik'))
			eb.baslangic_tarihi=tarih
			eb.aylik_ucret=float(request.POST.get('aylik_ucret'))
			eb.depozito=float(request.POST.get('depozito'))
			eb.minumum_kalis=int(request.POST.get('minumum_kalis'))
			eb.save()
			
			

			ed=Emlak_bilgileri.objects.get(pk=eb.id)
			ev_k=request.POST.getlist('ev_kolayliklari')
			oda_t=request.POST.getlist('oda_tipi')
			konut_t=request.POST.getlist('konut_tipi')
			banyo_t=request.POST.getlist('banyo_tipi')
			cinsiyet=request.POST.getlist('cinsiyet')
			mulk_k=request.POST.getlist('mulk_kurallari')
			for i in ev_k:
				ed.ev_kolayliklari.add(int(i))
			for i in oda_t:
				ed.oda_tipi.add(int(i))
			for i in konut_t:
				ed.konut_tipi.add(int(i))
			for i in banyo_t:
				ed.banyo_tipi.add(int(i))
			for i in cinsiyet:
				ed.cinsiyet.add(int(i))
			for i in mulk_k:
				ed.mulk_kurallari.add(int(i))
			ed.save()
			
			galeribaslik=eb.ilan_baslik.lower()			
			count=0
	
			if not (request.FILES):
		
				galeri=Galeri(
					e_bilgileri_id=int(ed.id),
					galeri_adi=galeribaslik,
					kapak_image="image/kapak/none.jpg"
					)
				galeri.save()
				dokuman=Dokumanlar()
				dokuman.galeri_id=galeri.id
				dokuman.image_url="image/kapak/none.jpg"	
				dokuman.save()
			for afile in request.FILES.getlist('image_url',""):
				if count==0:
					galeri=Galeri(
						e_bilgileri_id=int(ed.id),
						galeri_adi=galeribaslik,
						kapak_image=afile)
					galeri.save()
				count+=1
				dokuman=Dokumanlar()
				dokuman.galeri_id=galeri.id
				dokuman.image_url=afile		
				dokuman.save()
	
			return HttpResponseRedirect('/ilanlar')
		else:
			return render(request,'g_ilan_olustur.html',{'form':form})

	else:
		
		form=Emlak_bilgileri_Form()


		return render(request,'g_ilan_olustur.html',{'form':form})
	


	#ilan kayit
def detay(request,emlak_id):
	if request.method == 'POST':
		oy=request.POST.get('oy_ver')
		profil_id=request.POST.get('user_id')

		i=True
		try:
			Profil_ranked.objects.get(profil_id=profil_id)
		except Profil_ranked.DoesNotExist:
			i=False
			ranked=Profil_ranked(
					profil_id=profil_id,
					toplam=int(oy.encode('utf-8')),
					sayac=1,
					puan=int(oy.encode('utf-8'))
				)
			ranked.save()

		if i:	
		 	ranked=Profil_ranked.objects.get(profil_id=profil_id)
			ranked.toplam=ranked.toplam+int(oy.encode('utf-8'))
			ranked.sayac=ranked.sayac+1
			ranked.puan=int(ranked.toplam/ranked.sayac)
			ranked.save()
	em_bil=Emlak_bilgileri.objects.get(id=emlak_id)
	galeri=Galeri.objects.get(e_bilgileri_id=emlak_id)
	dokuman=Dokumanlar.objects.filter(galeri_id=galeri.id)
	k_tipi=Konut_Tipi.objects.filter(emlak_bilgileri=emlak_id)
	k_tipi = [i.__dict__ for i in k_tipi]
	konut_tipi=[]
	for i in k_tipi:
		konut_tipi.append(i['konut_tipi_ad'])

	o_tipi=Oda_Tipi.objects.filter(emlak_bilgileri=emlak_id)
	o_tipi = [i.__dict__ for i in o_tipi]
	oda_tipi=[]
	for i in o_tipi:
		oda_tipi.append(i['oda_tipi_ad'])

	cnsyt = Cinsiyet.objects.filter(emlak_bilgileri=emlak_id)
	cnsyt = [i.__dict__ for i in cnsyt]
	cinsiyet=[]
	for i in cnsyt:
		cinsiyet.append(i['cinsiyet_ad'])

	ev_kol = Ev_kolayliklari.objects.filter(emlak_bilgileri=emlak_id)
	ev_kol = [i.__dict__ for i in ev_kol]
	ev_kolayliklari=[]
	for i in ev_kol:
		ev_kolayliklari.append(i['ev_kolayliklari_ad'])

	mulk_kur = Mulk_kurallari.objects.filter(emlak_bilgileri=emlak_id)
	mulk_kur =[i.__dict__ for i in mulk_kur]
	mulk_kurallari=[]
	for i in mulk_kur:
		mulk_kurallari.append(i['mulk_kurallari_ad'])

	b_tipi= Banyo_tipi.objects.filter(emlak_bilgileri=emlak_id)
	b_tipi= [i.__dict__ for i in b_tipi]
	banyo_tipi=[]
	for i in b_tipi:
		banyo_tipi.append(i['banyo_tipi_ad'])

	adres1=em_bil.adres1+" "+em_bil.ilce+"/"+em_bil.sehir
	#paylasan kisi bilgisi
	profil_id=em_bil.profil_id
	user=User.objects.get(id=profil_id)
	profil=Profil_info.objects.get(user_id=user.id)
	ranked=Profil_ranked.objects.get(profil_id=profil.id)
	son_uyeler=Profil_info.objects.all()
	galeri_obj=Galeri.objects.all()
	galeri=[]
	uye=[]
	for x in son_uyeler[(len(son_uyeler)-5):len(son_uyeler)]:
		uye.append(x)
	for x in galeri_obj[(len(galeri_obj)-10):len(galeri_obj)]:
		galeri.append(x)
	
	return render(request,'g_ilan_detay.html',{'em_bil':em_bil,
		'konut_tipi':konut_tipi,
		'oda_tipi':oda_tipi,
		'cinsiyet':cinsiyet,
		'ev_kolayliklari':ev_kolayliklari,
		'mulk_kurallari':mulk_kurallari,
		'banyo_tipi':banyo_tipi,
		'dokuman':dokuman,
		'profil':profil,
		'ranked':range(ranked.puan),
		'puan':ranked.puan,
		'galeri':galeri,
		'uye':uye
		})
	#ilan detay

#yeni temada kullanılmıyor	
def profil_register(request):
	if request.method == 'POST':
		p_form = Profil_info_Form(request.POST,request.FILES)

		telefon=request.POST.get('telefon')
		tarih=request.POST.get('dogum_tarihi')
		t=tarih_kontrol(tarih)

		if p_form.is_valid() and t:
			p=p_form.save(commit=False)
			p.user=request.user
			p.save()
			return redirect('/profil')
		else:
			if not t:
				mesaj="Numara kurallarına dikkat ediniz!\nDoğum tarihi alınını uygun doldurun"
			else:
				mesaj="Numara kurallarına dikkat ediniz!"
			p_form = Profil_info_Form()
			return render(request, 'yeni_hesap_2.html',{'form':form,
				'mesaj':mesaj
				})
			

	else:
		form = Profil_info_Form()
		return render(request, 'yeni_hesap_2.html',{'form':form})

def register(request):

	if request.method == 'POST':
		p_form = Profil_info_Form(request.POST,request.FILES)
		form = Register_Form(request.POST)
		telefon=request.POST.get('telefon')
		tarih=request.POST.get('dogum_tarihi')
		t=tarih_kontrol(tarih)
		username=request.POST.get('username')
		password1=request.POST.get('password1')
		password2=request.POST.get('password2')
		if form.is_valid() and parola_kurallari(password1,password2,username)=="ok" and p_form.is_valid() and t:
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			login(request, user)
			p=p_form.save(commit=False)
			p.user=request.user
			p.save()
			return redirect('/profil')
		else:
			isim=False
			mesaj=""
			if not t:
				mesaj+="Hata:Numara kurallarına dikkat ediniz!\nDoğum tarihi alınını uygun doldurun"
			else:
				mesaj+="Uyarı:Numara kurallarına dikkat ediniz!"
			try:
				User.objects.get(username=username)
			except User.DoesNotExist:
				isim=True
        	if not isim:
        
        		mesaj+="\nUsername Zaten mevcut"
        		form = Register_Form()
        		p_form=Profil_info_Form()
        		return render(request, 'g_yeni_hesap_olustur.html', {'form': form,
        			'mesaj':mesaj,
        			'p_form':p_form
        			})
        	
        	elif isim and parola_kurallari(password1,password2,username)=="ok" and t:
        		mesaj+="\nUsername Alanı Uygun Olmayan karekter içeriyor!"
        		form = Register_Form()
        		p_form=Profil_info_Form()
        		return render(request, 'g_yeni_hesap_olustur.html', {'form': form,
        			'mesaj':mesaj,
        			'p_form':p_form
        			})
        	elif parola_kurallari(password1,password2,username)!="ok":
        		mesaj+=parola_kurallari(password1,password2,username)
        		form = Register_Form()
        		p_form=Profil_info_Form()
        		return render(request, 'g_yeni_hesap_olustur.html', {'form': form,
        			'p_form':p_form,
        			'mesaj':mesaj
        			})
			
	else:
		form = Register_Form()
		p_form=Profil_info_Form()
		return render(request, 'g_yeni_hesap_olustur.html', {'form': form,'p_form':p_form})

	
def parola_kurallari(password1,password2,username):
	if (password1==password2):
		if len(password1)>=8:
			if not password1.isdigit():
				if username in password1:
					return "Parola Kişisel bilgileri barındaramaz!"
				else :
					return "ok"
			else:
				return "Parola Tamamen sayısal Olamaz!"
		else:
			return "Parola 8 haneden küçük olamaz!"
	else:
		return "Parola Doğrulama Yanlış!"

	
	#kullanici kayit
def telefon_kontrol(telefon):
	if len(telefon)==11:
		if telefon.isdigit():
			return True
		else:
			return False
	else:
		return False

@login_required
def profil(request):
	mesaj=" "
	er_mesaj=" "
	if request.method == 'POST':
		
		mesaj="Bilgier başarıyla güncellendi"
		first_name=request.POST.get('first_name')
		last_name=request.POST.get('last_name')
		user_id=request.POST.get('user_id')
		meslek=request.POST.get('meslek')
		okul=request.POST.get('okul')
		telefon=request.POST.get('telefon')
		dogum_tarihi=request.POST.get('dogum_tarihi')
		biyografi=request.POST.get('biyografi')
		twitter=request.POST.get('twitter')
		facebook=request.POST.get('facebook')
		chck_resim=request.POST.get('resim')
		
		user=User.objects.get(id=request.user.id)
		profil_ = Profil_info.objects.get(id=user_id)
		
		user.first_name=first_name
		user.last_name=last_name
		if (chck_resim=="kaldir"):
			profil_.image_url='documents/no_image.png' 
		elif (len(request.FILES)==1):
			image_url=request.FILES['resim_alani']
			profil_.image_url=image_url
		else :
			pass
		profil_.meslek=meslek
		profil_.okul=okul
		if telefon_kontrol(telefon):
			profil_.telefon=telefon
		else:
			er_mesaj="\nTelefon alanı:(Format: '05555555555')"
		t=tarih_kontrol(dogum_tarihi)
		if t:
			profil_.dogum_tarihi=dogum_tarihi
		else:
			profil_.dogum_tarihi="1980-01-01"
			er_mesaj+="\nDoğum tarihine dikkatli bakın!"
		profil_.biyografi=biyografi
		profil_.twitter=twitter
		profil_.facebook=facebook
		profil_.save()
		user.save()

	profil=Profil_info.objects.get(user_id=request.user.id)
	d_tarih=str(profil.dogum_tarihi).split(" ")[0]

	return render(request,'g_profil.html',{'user':request.user,
		'profil':profil,
		'd_tarih':d_tarih,
		'mesaj':mesaj,
		'er_mesaj':er_mesaj
		})
	#kullanici profil
def butun_ilanlar(request):

	baslik="TÜM İLAN LİSTELERİ"
	em_bil=Emlak_bilgileri.objects.all()
	galeri=Galeri.objects.all()
	dokuman=Dokumanlar.objects.all()
	page = request.GET.get('page')
	paginator = Paginator(em_bil, 10)
	try:
		em_bil = paginator.page(page)
	except PageNotAnInteger:
		em_bil = paginator.page(1)
	except EmptyPage:
		em_bil = paginator.page(paginator.num_pages)
	return render(request,'g_tum_ilanlar.html',{'em_bil':em_bil,
		'galeri':galeri,
		'baslik':baslik,
		})
@login_required
def tum_ilanlar(request):
	em_bil=Emlak_bilgileri.objects.filter(profil=request.user)
	galeri=[]
	for i in em_bil:
		galeri.append(Galeri.objects.get(e_bilgileri_id=i.id))
	for  i in galeri:
		i.e_bilgileri.ilan_tarihi=str(i.e_bilgileri.ilan_tarihi).split(" ")[0]
		if len(i.e_bilgileri.ilan_baslik) >40:
			i.e_bilgileri.ilan_baslik=i.e_bilgileri.ilan_baslik[:40]+"..."
	page = request.GET.get('page', 1)
	paginator = Paginator(galeri, 10)
	try:
		galeri = paginator.page(page)
	except PageNotAnInteger:
		galeri = paginator.page(1)
	except EmptyPage:
		galeri = paginator.page(paginator.num_pages)
	return render(request,'g_ilan_listem.html',{'em_bil':em_bil,
		'galeri':galeri
		})
	#ilanlarim
def profilim(request,user_id):
	if request.method == 'POST':
		oy=request.POST.get('oy_ver')
		profil_id=request.POST.get('user_id')

		i=True
		try:
			Profil_ranked.objects.get(profil_id=profil_id)
		except Profil_ranked.DoesNotExist:
			i=False
			ranked=Profil_ranked(
					profil_id=profil_id,
					toplam=int(oy.encode('utf-8')),
					sayac=1,
					puan=int(oy.encode('utf-8'))
				)
			ranked.save()

		if i:	
		 	ranked=Profil_ranked.objects.get(profil_id=profil_id)
			ranked.toplam=ranked.toplam+int(oy.encode('utf-8'))
			ranked.sayac=ranked.sayac+1
			ranked.puan=int(ranked.toplam/ranked.sayac)
			ranked.save()
	
	profil=Profil_info.objects.get(user_id=user_id)
	c=True
	try:
		Profil_ranked.objects.get(profil_id=profil.id)
	except Profil_ranked.DoesNotExist:
		c=False
	if c:
		ranked=Profil_ranked.objects.get(profil_id=profil.id)
		puan=ranked.puan
	else:
		ranked=0
		puan=0
	em_bil=Emlak_bilgileri.objects.filter(profil_id=user_id)
	d_tarih=str(profil.dogum_tarihi).split(" ")[0]
	galeri=[]

	for i in em_bil:
		galeri.append(Galeri.objects.get(e_bilgileri_id=i.id))
	page = request.GET.get('page', 1)
	paginator = Paginator(em_bil, 10)
	try:
		em_bil = paginator.page(page)
	except PageNotAnInteger:
		em_bil = paginator.page(1)
	except EmptyPage:
		em_bil = paginator.page(paginator.num_pages)
	return render(request,'g_acente_detay.html',{'profil':profil,
		'em_bil':em_bil,
		'galeri':galeri,
		'd_tarih':d_tarih,
		'ranked':range(puan),
		'puan':puan
		})
	#profil detay
@csrf_exempt
def sign_in(request):
	return render_to_response('g_giris.html')
@csrf_exempt
def  auth_view(request):
	if request.method=='POST':
		username=request.POST.get('username','')
		password=request.POST.get('password','')
		user=authenticate(username=username,password=password)
		if user is not None:
			login(request, user)
			return redirect('/profil')
		else:
			hata_mesaji="Kullanıcı adı yada parola alanı yanlış!"
			return render_to_response('g_giris.html',{'hata_mesaji':hata_mesaji})
def cikis(request):
	logout(request)
	return redirect('/')

@login_required
def  ilan_sil(request,emlak_id):
	Emlak_bilgileri.objects.filter(id=emlak_id).delete()
	return redirect('/ilanlar')
@login_required
def update_password(request):
	user=User.objects.get(id=request.user)
#yeni temada kullanılmıyor
def ajente_listesi(request):


	profil=Profil_info.objects.all()
	galeri_obj=Galeri.objects.all()
	galeri=[]
	ranked=Profil_ranked.objects.all()
	for x in galeri_obj[:5]:
		galeri.append(x)
	page = request.GET.get('page', 1)
	paginator = Paginator(profil, 10)
	try:
		profil = paginator.page(page)
	except PageNotAnInteger:
		profil = paginator.page(1)
	except EmptyPage:
		profil = paginator.page(paginator.num_pages)
	return render(request,'acente_listeleri.html',{'profil':profil,
		'galeri':galeri,
		'ranked':ranked
		})
def ilan_guncelle(request,ilan_id):

	instance=Emlak_bilgileri.objects.get(id=ilan_id)
	form=Emlak_bilgileri_Form(request.POST or None,instance=instance)

	if request.method == 'POST':
		if form.is_valid():
			tarih=request.POST.get('baslangic_tarihi')
			
			eb=Emlak_bilgileri.objects.get(id=ilan_id)
			eb.ilan_detayii=request.POST.get('ilan_detayii',"")
			eb.ilan_baslik=request.POST.get('ilan_baslik')
			eb.adres1=request.POST.get('adres1')
			eb.adres2=request.POST.get('adres2',"")
			eb.ilce=request.POST.get('ilce')
			eb.sehir=request.POST.get('sehir')
			eb.posta_kodu=request.POST.get('posta_kodu',"")
			eb.oda_sayisi=int(request.POST.get('oda_sayisi'))
			eb.mutfak_sayisi=int(request.POST.get('mutfak_sayisi'))
			eb.banyo_sayisi=int(request.POST.get('banyo_sayisi'))
			eb.salon_sayisi=int(request.POST.get('salon_sayisi'))
			eb.ilan_tarihi=datetime.now()
			eb.website=request.POST.get('website',"")
			eb.yatak_oda_sayi=int(request.POST.get('yatak_oda_sayi'))
			eb.yatak_sayisi=int(request.POST.get('yatak_sayisi'))
			eb.genislik=int(request.POST.get('genislik'))
			eb.baslangic_tarihi=tarih
			eb.aylik_ucret=float(request.POST.get('aylik_ucret'))
			eb.depozito=float(request.POST.get('depozito'))
			eb.minumum_kalis=int(request.POST.get('minumum_kalis'))
			eb.save()
			
			

			ed=Emlak_bilgileri.objects.get(pk=eb.id)
			ev_k=request.POST.getlist('ev_kolayliklari')
			oda_t=request.POST.getlist('oda_tipi')
			konut_t=request.POST.getlist('konut_tipi')
			banyo_t=request.POST.getlist('banyo_tipi')
			cinsiyet=request.POST.getlist('cinsiyet')
			mulk_k=request.POST.getlist('mulk_kurallari')
			for i in ev_k:
				ed.ev_kolayliklari.add(int(i))
			for i in oda_t:
				ed.oda_tipi.add(int(i))
			for i in konut_t:
				ed.konut_tipi.add(int(i))
			for i in banyo_t:
				ed.banyo_tipi.add(int(i))
			for i in cinsiyet:
				ed.cinsiyet.add(int(i))
			for i in mulk_k:
				ed.mulk_kurallari.add(int(i))
			ed.save()
			
			galeribaslik=eb.ilan_baslik.lower()			
			count=0
			if len(request.FILES):
				for afile in request.FILES.getlist('image_url',""):
					if count==0:
						galeri=Galeri.objects.get(e_bilgileri_id=int(ed.id))
						galeri.galeri_adi=galeribaslik,
						galeri.kapak_image=afile
						galeri.save()
					count+=1
			
					dokuman=Dokumanlar()
					dokuman.galeri_id=galeri.id
					dokuman.image_url=afile		
					dokuman.save()
	
			return HttpResponseRedirect('/ilanlar/')
		else:
			return render(request,'g_ilan_guncelle.html',{'form':form,
				'instance':instance
				})
	else:
		return render(request,'g_ilan_guncelle.html',{'form':form,
					'instance':instance
				})	
def tarih_kontrol(tarih):
	date_format='%Y-%m-%d'
	gelen= datetime.strptime(tarih, date_format)
	suan=datetime.now()
	fark=suan-gelen
	if ((fark.days)/365)>17:
		return True
	else:
		return False
