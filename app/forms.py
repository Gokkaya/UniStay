# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from app.models import Ev_kolayliklari,Konut_Tipi,Cinsiyet,Mulk_kurallari,Banyo_tipi,Emlak_bilgileri,Oda_Tipi,Dokumanlar,Profil_info
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class DateInput(forms.DateInput):
    input_type = 'date'
class Emlak_bilgileri_Form(forms.ModelForm):
    my_default_errors = {
    'required': 'Bu alan boş bırakılamaz!',
    'invalid': 'Geçerli bir değer girin?'
    }

    _choices =  [(int(m.id), m.ev_kolayliklari_ad) for m in Ev_kolayliklari.objects.all()]
    _choices2 = [(int(m.id), m.oda_tipi_ad) for m in Oda_Tipi.objects.all()]
    _choices3 = [(int(m.id), m.konut_tipi_ad) for m in Konut_Tipi.objects.all()]
    _choices4 = [(int(m.id), m.cinsiyet_ad) for m in Cinsiyet.objects.all()]
    _choices5 = [(int(m.id), m.mulk_kurallari_ad) for m in Mulk_kurallari.objects.all()]
    _choices6 = [(int(m.id), m.banyo_tipi_ad) for m in Banyo_tipi.objects.all()]
    
    banyo_tipi     =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices6,required=True,error_messages=my_default_errors)
    mulk_kurallari =forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=_choices5,required=True,error_messages=my_default_errors)
    cinsiyet       =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices4,  required=True,error_messages=my_default_errors)
    konut_tipi     =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices3,  required=True,error_messages=my_default_errors)
    oda_tipi       =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices2,required=True,error_messages=my_default_errors)
    ev_kolayliklari=forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=_choices,required=True,error_messages=my_default_errors)
    aylik_ucret      = forms.FloatField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    depozito         = forms.FloatField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    minumum_kalis    = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    
    oda_sayisi       = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    mutfak_sayisi    = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    banyo_sayisi     = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    salon_sayisi     = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    yatak_oda_sayi   = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    yatak_sayisi     = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    genislik         = forms.IntegerField(required=True,error_messages=my_default_errors,widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    ilan_detayii     = forms.CharField(required=False,widget=forms.Textarea(attrs={'class' : 'form-control'}))
    ilan_baslik      = forms.CharField(required=True,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    adres1           = forms.CharField(required=True,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    adres2           = forms.CharField(required=False,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    ilce             = forms.CharField(required=True,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    sehir            = forms.CharField(required=True,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    posta_kodu       = forms.CharField(required=False,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    website          = forms.CharField(required=False,error_messages=my_default_errors,widget=forms.TextInput(attrs={'class' : 'form-control'}))

    class Meta:
        model = Emlak_bilgileri
        fields=('baslangic_tarihi',
            'aylik_ucret',
            'depozito',
            'minumum_kalis',
            'oda_sayisi',
            'mutfak_sayisi',
            'banyo_sayisi',
            'salon_sayisi',
            'yatak_oda_sayi',
            'yatak_sayisi',
            'genislik',
            'ev_kolayliklari',
            'oda_tipi',
            'konut_tipi',
            'banyo_tipi',
            'cinsiyet',
            'mulk_kurallari',
            'ilan_detayii',
            'ilan_baslik',
            'adres1',
            'adres2',
            'ilce',
            'sehir',
            'posta_kodu',
            'website')
        widgets = {
            'baslangic_tarihi': DateInput()
        }
class Filtre_Form(forms.ModelForm):
 

    
    _choices  = [(int(m.id), m.ev_kolayliklari_ad) for m in Ev_kolayliklari.objects.all()]
    _choices2 = [(int(m.id), m.oda_tipi_ad) for m in Oda_Tipi.objects.all()]
    _choices3 = [(int(m.id), m.konut_tipi_ad) for m in Konut_Tipi.objects.all()]
    _choices4 = [(int(m.id), m.cinsiyet_ad) for m in Cinsiyet.objects.all()]
    _choices5 = [(int(m.id), m.mulk_kurallari_ad) for m in Mulk_kurallari.objects.all()]
    _choices6 = [(int(m.id), m.banyo_tipi_ad) for m in Banyo_tipi.objects.all()]
    
    banyo_tipi     =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices6,required=False)
    mulk_kurallari =forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,required=False)
    cinsiyet       =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices4,required=False)
    konut_tipi     =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices3,required=False)
    oda_tipi       =forms.ChoiceField(widget=forms.RadioSelect, choices=_choices2,required=False)
    ev_kolayliklari=forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=_choices,required=False)
    class Meta:
        model = Emlak_bilgileri
        fields=(
            'banyo_tipi',
            'ev_kolayliklari',
            'oda_tipi',
            'konut_tipi',
            'banyo_tipi',
            'cinsiyet',
            'mulk_kurallari'
            )
  
class Register_Form(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, label="Ad", help_text='Zorunlu Alan*',widget=forms.TextInput(attrs={'class' : 'form-control'}))
    last_name = forms.CharField(max_length=30, required=True, help_text='Zorunlu Alan*',label="Soyad",widget=forms.TextInput(attrs={'class' : 'form-control'}))
    email = forms.EmailField(max_length=254, help_text='Mail adresinizi giriniz',label="Email",required=True,widget=forms.EmailInput(attrs={'class' : 'form-control'}))
    username=forms.CharField(max_length=150, required=True, label="Username",help_text="Max 150 karekter olmalıdır ve sadece hafler,sayılar ve @/./+/-/_  içerebilir*",widget=forms.TextInput(attrs={'class' : 'form-control'})) 
    password1=forms.CharField(required=True,label="Parola",help_text="Şifreniz diğer kişisel bilgilerinize çok benzemez.\nŞifreniz en az 8 karakter içermelidir.\nŞifreniz sık kullanılan bir şifre olamaz.\nŞifreniz tamamen sayısal olamaz.",widget=forms.PasswordInput(attrs={'class' : 'form-control'}))
    password2=forms.CharField(required=True,label="Parola Tekrarı",widget=forms.PasswordInput(attrs={'class' : 'form-control'}))
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class DocumentForm(forms.ModelForm):
    image_url = forms.FileField()
    class Meta:
        model = Dokumanlar
        fields = ('image_url',)

class Profil_info_Form(forms.ModelForm):
    dogum_tarihi=forms.DateInput()
    meslek=forms.CharField(required=True,help_text="Zorunlu*",widget=forms.TextInput(attrs={'class' : 'form-control'}))
    okul=forms.CharField(required=True,help_text="Zorunlu*",widget=forms.TextInput(attrs={'class' : 'form-control'}))
    biyografi=forms.CharField(required=False,widget=forms.Textarea(attrs={'class' : 'form-control'}))
    facebook=forms.CharField(required=False,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    twitter=forms.CharField(required=False,widget=forms.TextInput(attrs={'class' : 'form-control'}))
    telefon=forms.RegexField(required=True,regex=r'^\+?1?\d{9,15}$', help_text="Format: '05555555555'",widget=forms.TextInput(attrs={'class' : 'form-control'}))
    image_url=forms.FileField(required=False,label="Profil Resmi",help_text="Zorunlu değil",widget=forms.FileInput(attrs={'class' : 'form-control'}))

    class Meta:
        model=Profil_info
        fields=('image_url','telefon','meslek','okul','dogum_tarihi','biyografi','facebook','twitter')
        widgets = {
            'dogum_tarihi': DateInput()
        }

class User_Form(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model=User
        fields=('username','password','first_name','last_name','email')